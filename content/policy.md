## Privacy Policy

#### 1. Name and contact details of the data controller

This data privacy information informs you about the processing of your personal data (hereinafter "data") in connection with the use of our mbioSphere. Responsible for the data processing is micro-biolytics GmbH, Schelztorstrasse 54-56, 73728 Esslingen am     Neckar, Germany ("MBIO"). Further information about MBIO can be found in our imprint. If you have any questions regarding data protection, please contact info@micro-biolytics.com.

#### 2. Use of mbioSphere

In order to use our services, you must set up a personal user account (hereinafter "user account"). 

In order to set up the user account, you must provide a professional email address and a carefully chosen password which must be kept confidential at all times. Upon providing this data, you can conclude the agreement that provides the user account.

A service component of the user account is the Single Sign-on Login service of AuthO, 10900 NE 8th Street, Suite 700, Bellevue, WA 98003 USA ("AuthO"). We transmit your user data to AuthO in the USA.  We have concluded the EU standard contractual clauses 2010/87 (EU) with AuthO in order to ensure an adequate level of data protection. Further information on the protection of data privacy by AuthO can be found at https://auth0.com/privacy/.

AuthO authenticates the identity of the user via the single sign-on service for all log-in procedures, whereby the user only has to enter his email address and password for log-in. The user has to to keep the data up-to-date in his own interest. The user is responsible for all processes initiated under his username.

After logging into a set up user account, you may access all of MBIO’s apps in mbioSphere. 

You may delete your user account at any time and without giving reasons. The easiest way to do this is by sending an informal e-mail to info@micro-biolytics.com. We will delete your data as soon as we have received your request to delete your user account unless legal regulations request and/or entitle us to further storage of your personal data.

The legal basis for the data processing associated herewith is Art. 6 (1) lit. b GDPR (fulfilment of contract).

#### 3. Transfer of data

Your personal data will not be transferred to third parties for other purposes than those listed below.

We only pass on users’ data to third parties, if

* the user has given consent in accordance with Art. 6 (1) lit. a GDPR,
* the processing is necessary for the performance of a contract or for carrying out pre-contractual measures referred to in Article 6 (1) lit. b GDPR,
* the processing is necessary for the fulfilment of a legal obligation pursuant to Art. 6 (1) lit. c GDPR,
* the disclosure is necessary in accordance with Art. 6 (1) lit. f GDPR to ensure our legitimate interests and there is no reason to assume that the user has an overriding interest worthy of protection in not disclosing the data.

Apart from the above, we engage – if applicable – carefully selected, supervised data processors which are subject to our instructions, for the performance of our services, in particular in order to provide the software application that is necessary to use the Analyzer via Software as a Service („SaaS“) if applicable, for hosting and/or storing of personal data as well as with regard to IT security and system control.

The provider for our software application is DigitalOcean, LLC („DigitalOcean“). You will find further information regarding the protection of your data by DigitalOcean under https://www.digitalocean.com/legal/. 

#### 4. Rights of data subjects

You have the right

* in accordance with Art. 15 GDPR to request information about your personal data processed by us. In particular, you may request information on the purposes of the processing, the categories of such data, the categories of recipients to whom your data has been or will be disclosed, the planned duration of storage, the existence of a right of rectification, cancellation, restriction of processing or opposition, the existence of a right of appeal, the origin of your data if it has not been collected by us, as well as the existence of automated decision making including profiling and, if applicable, meaningful information on the details thereof;
* in accordance with Art. 16 GDPR to demand the correction of incorrect or incomplete data stored by us without delay;
* in accordance with Art. 17 GDPR to demand the deletion of your data stored with us, unless the processing is necessary to exercise the right to freedom of expression and information, to fulfil a legal obligation, for reasons of public interest or to assert, exercise or defend legal claims;
* in accordance with Art. 18 GDPR to demand the restriction of the processing of your data, insofar as the accuracy of the data is disputed by you, the processing is unlawful, but you refuse to delete it and we no longer require the data, but you require it for the assertion, exercise or defense of legal claims or you have lodged an objection to the processing in accordance with Art. 21 GDPR;
* in accordance with Art. 20 GDPR, to receive your data that you have provided to us in a structured, common and machine-readable format or to request that it be transferred to another responsible party;
* in accordance with Art. 7 (3) GDPR, to revoke any consent you may have given to us at any time. As a result, we may no longer continue data processing based on this consent for the future; and
* to complain to a supervisory authority in accordance with Art. 77 GDPR. In general, you can contact the supervisory authority of your usual place of residence or workplace or of our head office.

#### 5. Right of objection

If your data is processed on the basis of legitimate and overriding interests in accordance with Art. 6 (1) lit. f DSGVO, you have the right to object to the processing of your data in accordance with the provisions of Art. 21 DSGVO.

If you decide to exercise your right of objection, please send an e-mail to info@micro-biolytics.com.

#### 6. Data security

We use appropriate technical and organizational security measures to protect your data against accidental or intentional manipulation, partial or complete loss, destruction or unauthorized access by third parties. Our security measures are continuously improved in line with technological developments.
